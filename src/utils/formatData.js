export default function (data) {
    return new Date(data).toLocaleDateString();
}