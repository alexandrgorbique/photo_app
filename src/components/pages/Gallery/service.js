import settings from '../../../system/setings.json';

export const getApiPhotos = () =>
    fetch(settings.url)
    .then((response) => {
        return response.json();
    }) 
