import React, {Component} from 'react';
import { connect } from 'react-redux'
import ListPhotos from './ListPhotos'
import './style.scss';

import { callApiPhotosAction} from './actions';

class Gallery extends Component{
    
    componentDidMount() {
       this.props.callApiPhotos();
            
        
    }
    render() {
        const { photos } = this.props;
    return (
        <div id="gallery" className="page">
            <h1>Gallery </h1>
            <ListPhotos photos={photos}/>
        </div>
    )
    }
}
const mapStateToProps = (state) => ({
    photos: state.gallery.photos,
})
const mapDispatchToProps = (dispatch) => ({
  
    callApiPhotos: () => callApiPhotosAction ( dispatch),
})
export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
