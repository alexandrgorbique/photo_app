import { getApiPhotos } from './service';

export const setPhotosActionType = photos => ({ type: 'SET_PHOTOS', photos });
export const callApiPhotosAction = dispatch => {
    const photosData = getApiPhotos();
    

    photosData.then(photos => {
        dispatch(setPhotosActionType(photos))
        localStorage.setItem( 'photos', JSON.stringify(photos))

    })
}