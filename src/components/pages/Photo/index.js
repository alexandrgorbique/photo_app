import React from 'react';
import { connect } from 'react-redux'
import formatDate from '../../../utils/formatData';
import { getPhoto } from './selectors'
import './style.scss';

const Photo = ({data}) => {
    const {
        id,
        urls: { raw },
        created_at,
    } = data;

    return (
        <div id="photo" className="page">
            <h1>{formatDate(created_at)}</h1>
            <img key={id} src={raw} alt='' />

        </div>
            
    )
}

const mapStateToProps = (state, ownProps) => ({
    data: getPhoto(state, ownProps)

});

export default connect(mapStateToProps)(Photo);