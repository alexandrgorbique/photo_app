import React from 'react';
import Home from '../pages/Home';
import Gallery from '../pages/Gallery';
import Contacts from '../pages/Contacts';
import Photo from '../pages/Photo';
import { Switch, Route } from 'react-router-dom';


export default function () {
    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/gallery' component={Gallery} />
            <Route path='/contacts' component={Contacts} />
            <Route path='/photo/:id' component={Photo} />
        </Switch>
    )
}