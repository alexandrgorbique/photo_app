import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Router from './components/Router';
import Header from './components/Header';
import Footer from './components/Footer';
import './App.scss';

class App extends Component {

  render() {
    console.log('APP render');
    return (
      <BrowserRouter>
      <div className="App">
               <h1>Hello User</h1>
        <Header/>
        <Router/>
        <Footer />
      </div>
        </BrowserRouter>
    );
  }
}


export default App;
